# Mongoid 3.* 和 2.* 更改了api，有些方法名字改了，还有mapreduce可以链式调用了
# 在修复mongoid-mapreduce时总结了手工连接mongodb自定义数据库的方法

#  连接指定数据库
Mongoid.connect_to "mongoid-mapreduce-test"

# drop除系统自带的collections
Mongoid.default_session.collections.select {|c| c.name !~ /system/ }.each(&:drop)